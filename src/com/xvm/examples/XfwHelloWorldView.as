/**
 * XFW Hello World
 * @author Maxim Schedriviy <max(at)modxvm.com>
 */
package com.xvm.examples
{
    import com.xfw.infrastructure.*;
    import flash.text.*;
    import flash.display.*;
    import net.wg.gui.battle.random.views.*;
    import net.wg.infrastructure.interfaces.*;
    import net.wg.infrastructure.events.*;
    import scaleform.gfx.*;

    public class XfwHelloWorldView extends XfwViewBase
    {
        [Embed(source="../../../../assets/xvm-96x96.png")]
        private var XvmImageClass:Class;
        private var xvmImage:Bitmap = new XvmImageClass();

        public function XfwHelloWorldView(view:IView)
        {
            DebugUtils.LOG_DEBUG("XfwHelloWorldView");
            super(view);
        }

        public function get page():BattlePage
        {
            return super.view as BattlePage;
        }

        override public function onAfterPopulate(e:LifeCycleEvent):void
        {
            super.onAfterPopulate(e);

            var behindMinimapIndex:int = page.getChildIndex(page.minimap) - 1;

            var textField:TextField = new TextField();
            textField.selectable = false;
            textField.mouseEnabled = false;
            textField.width = App.appWidth;
            textField.height = App.appHeight;
            textField.defaultTextFormat = new TextFormat("$FieldFont", 50, 0xFF0000, true, null, null, null, null, TextFormatAlign.CENTER);
            textField.autoSize = TextFieldAutoSize.CENTER;
            TextFieldEx.setVerticalAutoSize(textField, TextFieldEx.VAUTOSIZE_CENTER);
            TextFieldEx.setVerticalAlign(textField, TextFieldEx.VALIGN_CENTER);
            TextFieldEx.setImageSubstitutions(textField, [{subString:"{XVM}", image:xvmImage.bitmapData, width:96, height:96, id:"xvm"}]);
            textField.htmlText = "{XVM}\nHello, world!\nXFW mod example";
            page.addChildAt(textField, behindMinimapIndex);
        }
    }
}
