/**
 * XFW Hello World
 * @author Maxim Schedriviy <max(at)modxvm.com>
 */
package com.xvm.examples
{
    import com.xfw.infrastructure.*;

    public class XfwHelloWorldMod extends XfwModBase
    {
        public function XfwHelloWorldMod()
        {
            super();
            DebugUtils.LOG_DEBUG("XFW: Hello, world!");
        }

        private static const _views:Object =
        {
            "classicBattlePage": [ XfwHelloWorldView ]
        }

        public override function get views():Object
        {
            return _views;
        }
    }
}
