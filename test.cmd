@echo off

set WOT_DIR=D:\work\games\WoT
set REPLAY=test.wotreplay

if exist "%WOT_DIR%" (
    xcopy /Y bin\xfw_hello_world.swf %WOT_DIR%\res_mods\mods\packages\xfw_hello_world\as_battle\
    start %REPLAY%
)
